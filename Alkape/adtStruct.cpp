#include "adtStruct.h"

ADT_MVER::ADT_MVER(ifstream &adtFile)
{
	adtFile.read((char*)&m_size, sizeof(m_size));
	adtFile.read((char*)&m_version, sizeof(m_version));
}

ADT_MVER::~ADT_MVER()
{
}

ADT_MHDR::ADT_MHDR(ifstream &adtFile)
{
	adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // size = 64 bytes
	
	adtFile.read((char*)&m_flags, sizeof(m_flags));
	adtFile.read((char*)&m_ofsMCIN, sizeof(m_ofsMCIN));
	adtFile.read((char*)&m_ofsMTEX, sizeof(m_ofsMTEX));
	adtFile.read((char*)&m_ofsMMDX, sizeof(m_ofsMMDX));
	adtFile.read((char*)&m_ofsMMID, sizeof(m_ofsMMID));
	adtFile.read((char*)&m_ofsMWMO, sizeof(m_ofsMWMO));
	adtFile.read((char*)&m_ofsMWID, sizeof(m_ofsMWID));
	adtFile.read((char*)&m_ofsMDDF, sizeof(m_ofsMDDF));
	adtFile.read((char*)&m_ofsMODF, sizeof(m_ofsMODF));
	adtFile.read((char*)&m_ofsMFBO, sizeof(m_ofsMFBO));
	adtFile.read((char*)&m_ofsMH2O, sizeof(m_ofsMH2O));
	adtFile.read((char*)&m_ofsMTFX, sizeof(m_ofsMTFX));
	adtFile.read((char*)&m_unused, sizeof(m_unused));
}

ADT_MHDR::~ADT_MHDR()
{
}

ADT_MCIN::ADT_MCIN(ifstream & adtFile)
{
	adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // size = 4096 bytes
	
	for (int i = 0; i < 16; i++)
	{
		vector<McinInfo> row;

		for (int j = 0; j < 16; j++)
		{
			McinInfo mcinInfo;
			adtFile.read((char*)&mcinInfo, sizeof(McinInfo));
			row.push_back(mcinInfo);
		}

		m_chunkIndex.push_back(row);
	}
}

ADT_MCIN::~ADT_MCIN()
{
}

::uint32_t ADT_MCIN::GetFirstOfsMCNK()
{
	return m_chunkIndex[0][0].ofsMCNK;
}

ADT_MTEX::ADT_MTEX(ifstream & adtFile)
{
	string texs;
	adtFile.read((char*)&m_size, sizeof(m_size));

	if (m_size > 0)
	{
		texs.resize(m_size);
		adtFile.read(&texs[0], m_size);

		size_t pos = 0;
		string token;
		istringstream ss(texs);
		while (getline(ss, token, '\0'))
		{
			//cout << "ADT_MTEX token : " << token << endl;
			m_texFileList.push_back(token);
		}
	}
}

ADT_MTEX::~ADT_MTEX()
{
}

ADT_MMDX::ADT_MMDX(ifstream & adtFile)
{
	string m2s;
	adtFile.read((char*)&m_size, sizeof(m_size));

	if (m_size > 0)
	{
		m2s.resize(m_size);
		adtFile.read(&m2s[0], m_size);

		size_t pos = 0;
		string token;
		istringstream ss(m2s);
		while (getline(ss, token, '\0'))
		{
			//cout << "ADT_MMDX token : " << token << endl;
			m_doodadFileList.push_back(token);
		}
	}
}

ADT_MMDX::~ADT_MMDX()
{
}

ADT_MMID::ADT_MMID(ifstream & adtFile)
{
	::uint32_t size;
	adtFile.read((char*)&size, sizeof(size));

	//cout << "Bite MMID size :" << size << endl;
	//cout << "Bite 1 tellg : " << adtFile.tellg() << endl;
	for (int i = 0; i < size/4; i++) // 4 (32bits) * nOfs
	{
		::uint32_t ofs;
		adtFile.read((char*)&ofs, sizeof(ofs));
		m_ofsDoodadFileList.push_back(ofs);
	}

	//cout << "Bite 2 tellg : " << adtFile.tellg() << endl;
}

ADT_MMID::~ADT_MMID()
{
}

ADT_MWMO::ADT_MWMO(ifstream & adtFile)
{
	string wmos;
	adtFile.read((char*)&m_size, sizeof(m_size));

	if (m_size > 0)
	{
		wmos.resize(m_size);
		adtFile.read(&wmos[0], m_size);

		size_t pos = 0;
		string token;
		istringstream ss(wmos);
		while (getline(ss, token, '\0'))
		{
			m_wmoFileList.push_back(token);
		}
	}
}

ADT_MWMO::~ADT_MWMO()
{
}

ADT_MWID::ADT_MWID(ifstream & adtFile)
{
	::uint32_t size;
	adtFile.read((char*)&size, sizeof(size));

	for (int i = 0; i < size/4; i++)
	{
		::uint32_t ofs;
		adtFile.read((char*)&ofs, sizeof(ofs));
		m_ofsWmoFileList.push_back(ofs);
	}
}

ADT_MWID::~ADT_MWID()
{
}

ADT_MDDF::ADT_MDDF(ifstream & adtFile)
{
	::uint32_t size;
	adtFile.read((char*)&size, sizeof(size));

	for (int i = 0; i < (size/36); i++)
	{
		MddfInfo mddfInfo;
		adtFile.read((char*)&mddfInfo, sizeof(mddfInfo));
		m_doodadDefList.push_back(mddfInfo);
	}
}

ADT_MDDF::~ADT_MDDF()
{
}

ADT_MODF::ADT_MODF(ifstream & adtFile)
{
	::uint32_t size;
	adtFile.read((char*)&size, sizeof(size));

	for (int i = 0; i < (size / 64); i++)
	{
		ModfInfo mddfInfo;
		adtFile.read((char*)&mddfInfo, sizeof(mddfInfo));
		m_wmoDefList.push_back(mddfInfo);
	}
}

ADT_MODF::~ADT_MODF()
{
}

ADT_MCNK::ADT_MCNK(ifstream & adtFile)
{
	::uint32_t size;
	string identifier;
	identifier.resize(4);

	//cout << "tellg : " << hex << adtFile.tellg() << endl;

	adtFile.read((char*)&m_header, sizeof(m_header));

	//cout << "tellg : " << hex << adtFile.tellg() << endl;
	// MCVT
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "TVCM")
	{
		cout << "Error : Chunk identifier not found MCVT : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	adtFile.read((char*)&m_mcvt, sizeof(m_mcvt));

	// MCNR
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "RNCM")
	{
		cout << "Error : Chunk identifier not found MCNR : " << identifier << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	adtFile.read((char*)&m_mcnr, sizeof(m_mcnr));
	::uint8_t padding[13]; // unused
	adtFile.read((char*)&padding, sizeof(padding));

	// MCLY
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "YLCM")
	{
		cout << "Error : Chunk identifier not found MCLY : " << identifier << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	for (int i = 0; i < m_header.nLayers; i++)
	{
		MclyEntry mclyEntry;
		adtFile.read((char*)&mclyEntry, sizeof(mclyEntry)); // 1-4 layers
		m_mcly.mclyList.push_back(mclyEntry);
	}

	// MCRF
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "FRCM")
	{
		cout << "Error : Chunk identifier not found MCRF : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	for (int i = 0; i < m_header.nDoodadRefs; i++)
	{
		::uint32_t doodadRef;
		adtFile.read((char*)&doodadRef, sizeof(doodadRef));
		m_mcrf.doodadRefs.push_back(doodadRef);
	}
	for (int i = 0; i < m_header.nMapObjRefs; i++)
	{
		::uint32_t wmoRef;
		adtFile.read((char*)&wmoRef, sizeof(wmoRef));
		m_mcrf.doodadRefs.push_back(wmoRef);
	}

	// MCSH
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "HSCM")
	{
		cout << "Error : Chunk identifier not found MCSH : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	adtFile.read((char*)&m_mcsh, sizeof(m_mcsh));

	// MCAL
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "LACM")
	{
		cout << "Error : Chunk identifier not found MCAL : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}
	adtFile.read((char*)&size, sizeof(size));
	if (size > 0)
	{
		for (int i = 0; i < (size / 2048); i++) // 0-3 layers * 2048 bytes
		{
			McalEntry mcalEntry;
			adtFile.read((char*)&mcalEntry, sizeof(mcalEntry));
			m_mcal.alphaMap.push_back(mcalEntry);
		}
	}

	// MCLQ - Deprecated in WotLK, only for retro-compatibility
	adtFile.read((char*)&identifier, sizeof(::uint32_t));
	if (identifier != "QLCM" && identifier != "ESCM") // Ignored
	{
		cout << "Error : Chunk identifier not found MCLQ or MCSE : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}

	// MCSE - Too poor doc, we will see later
}

ADT_MCNK::~ADT_MCNK()
{
}

float ADT_MCNK::GetValNoLOD(int x, int y)
{
	return m_mcvt.height[GetIdNoLOD(x, y)];
}

float ADT_MCNK::GetValLOD(int x, int y)
{
	return  m_mcvt.height[GetIdLOD(x, y)];
}

Vector3f ADT_MCNK::GetPosition()
{
	//Vector3f pos(m_header.posx, m_header.posy, m_header.posz);
	Vector3f pos(-m_header.posy, m_header.posx, m_header.posz);
	return pos;
}

int ADT_MCNK::GetIdNoLOD(int x, int y)
{
	return x + y * 17;
}

int ADT_MCNK::GetIdLOD(int x, int y)
{
	return 9 + y * 17 + x;
}