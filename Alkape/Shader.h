#ifndef DEF_SHADER
#define DEF_SHADER

#include <SFML/OpenGL.hpp>

// Include Windows

#ifdef WIN32
#include <GL/glew.h>


// Include Mac

#elif __APPLE__
#define GL3_PROTOTYPES 1
#include <OpenGL/gl3.h>


// Include UNIX/Linux

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif


// Includes communs

#include <iostream>
#include <string>
#include <fstream>


// Classe Shader

class ShaderTuto
{
    public:

		ShaderTuto();
		ShaderTuto(ShaderTuto const &shaderACopier);
		ShaderTuto(std::string vertexSource, std::string fragmentSource);
    ~ShaderTuto();

	ShaderTuto& operator=(ShaderTuto const &shaderACopier);

    bool charger();
    bool compilerShader(GLuint &shader, GLenum type, std::string const &fichierSource);
    GLuint GetVertexID() const;
	GLuint GetFragmentID() const;
	GLuint GetProgramID() const;

	std::string m_vertexSource;
	std::string m_fragmentSource;

    private:

    GLuint m_vertexID;
    GLuint m_fragmentID;
    GLuint m_programID;
};

#endif
