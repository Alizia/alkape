#pragma once

#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <cmath>
#include <sstream>
#include <bitset>

#include <SFML\System.hpp>

using namespace std;
using namespace sf;

class ADT_MVER
{
	public:
		ADT_MVER(ifstream &adtFile);
		~ADT_MVER();

	private:
		::uint32_t m_size; // useless, always 0x04 ?
		::uint32_t m_version;
};

class ADT_MHDR
{
	public:
		ADT_MHDR(ifstream &adtFile);
		~ADT_MHDR();

	private:
		::uint32_t m_flags;   // 1 = contains MFBO
		::uint32_t m_ofsMCIN;
		::uint32_t m_ofsMTEX;
		::uint32_t m_ofsMMDX;
		::uint32_t m_ofsMMID;
		::uint32_t m_ofsMWMO;
		::uint32_t m_ofsMWID;
		::uint32_t m_ofsMDDF;
		::uint32_t m_ofsMODF;
		::uint32_t m_ofsMFBO; // only if flags = 1.
		::uint32_t m_ofsMH2O;
		::uint32_t m_ofsMTFX;
		::uint32_t m_unused[4];
};

struct McinInfo
{
	::uint32_t ofsMCNK;
	::uint32_t size;
	::uint32_t flags; // 1 = loaded by the client
	::uint32_t asyncId; // unused ?
};

class ADT_MCIN
{
	public:
		ADT_MCIN(ifstream &adtFile);
		~ADT_MCIN();

		::uint32_t GetFirstOfsMCNK();

	private:
		vector<vector<McinInfo>> m_chunkIndex;
};

class ADT_MTEX
{
	public:
		ADT_MTEX(ifstream &adtFile);
		~ADT_MTEX();

	private:
		::uint32_t m_size;
		vector<string> m_texFileList;
};

class ADT_MMDX
{
	public:
		ADT_MMDX(ifstream &adtFile);
		~ADT_MMDX();

	private:
		::uint32_t m_size;
		vector<string> m_doodadFileList;
};

class ADT_MMID
{
	public:
		ADT_MMID(ifstream &adtFile);
		~ADT_MMID();

	private:
		vector<::uint32_t> m_ofsDoodadFileList;
};

class ADT_MWMO
{
	public:
		ADT_MWMO(ifstream &adtFile);
		~ADT_MWMO();

	private:
		::uint32_t m_size;
		vector<string> m_wmoFileList;
};

class ADT_MWID
{
	public:
		ADT_MWID(ifstream &adtFile);
		~ADT_MWID();

	private:
		vector<::uint32_t> m_ofsWmoFileList;
};

struct MddfInfo
{
	::uint32_t mmidEntry;
	::uint32_t uniqueId;
	Vector3f position;
	Vector3f orientation;
	::uint16_t scale;
	::uint16_t flags;
};

class ADT_MDDF
{
	public:
		ADT_MDDF(ifstream &adtFile);
		~ADT_MDDF();

	private:
		vector<MddfInfo> m_doodadDefList;
};

struct ModfInfo
{
	::uint32_t nameId;
	::uint32_t uniqueId;
	Vector3f position;
	Vector3f orientation;
	float upperExtents[3];
	float lowerExtents[3];
	::uint16_t flags;
	::uint16_t doodadSetIndex;
	::uint16_t nameSet;
	::uint16_t padding;
};

class ADT_MODF
{
	public:
		ADT_MODF(ifstream &adtFile);
		~ADT_MODF();

	private:
		vector<ModfInfo> m_wmoDefList;
};

/*class ADT_MH2O // To do; not needed for test.
{
	public:
		ADT_MH2O(ifstream &wdtFile);
		~ADT_MH2O();

	private:

};*/

struct McnkHeader
{
	::uint32_t flags; // 1h=has MCSH, 2h=impassible, 4h=River, 8h=Ocean, 10h=Magma, 20h=Slime, 40h=has MCCV
	::uint32_t indexX;
	::uint32_t indexY;
	::uint32_t nLayers; // maximum 4
	::uint32_t nDoodadRefs;
	::uint32_t ofsMCVT; // offsets to various chunks. Relative to the beginning of the MCNK Chunk
	::uint32_t ofsMCNR;
	::uint32_t ofsMCLY;
	::uint32_t ofsMCRF;
	::uint32_t ofsMCAL;
	::uint32_t sizeAlpha;
	::uint32_t ofsMCSH; // only with flags&0x1, but present sometimes even if don't exist... Fuck you Blizzard.
	::uint32_t sizeShadow;
	::uint32_t areaid;
	::uint32_t nMapObjRefs;
	::uint32_t holes;
	::uint64_t reallyLowQualityTextureingMap1; // the content is the layer being on top, I guess.
	::uint64_t reallyLowQualityTextureingMap2;
	::uint64_t noEffectDoodad; // doodads disabled if 1
	::uint32_t ofsMCSE;
	::uint32_t nSndEmitters; // will be set to 0 in the client if ofsSndEmitters doesn't point to MCSE!
	::uint32_t ofsMCLQ;
	::uint32_t sizeLiquid; // 8 when not used; only read if >8.
	//Vector3f   position;
	  float    posx;
	  float    posy;
	  float    posz;
	::uint32_t ofsMCCV;
	::uint32_t unused;
	::uint32_t unused2;
};

struct Mcvt
{
	float height[9 * 9 + 8 * 8];
};

struct Mcnr
{
	::uint8_t normals[(9 * 9 + 8 * 8) * 3]; // normalized. X, Z, Y. 127 == 1.0, -127 == -1.0. 
};

struct MclyEntry
{
	::uint32_t texId;
	::uint32_t flags;
	::uint32_t offsetInMCAL;
	::int32_t  effectId; // (actually int16 and padding)
};

struct Mcly
{
	vector<MclyEntry> mclyList;
};

struct Mcrf
{
	vector<::uint32_t> doodadRefs;
	vector<::uint32_t> wmoRefs;
};

struct Mcsh // Can be left out with the chunk&1 flag not set.
{
	::uint64_t shadowMap[64];
};

struct McalEntry
{
	::uint8_t mcalEntry[32][64]; // 2 alpha values per byte. Each layer contains a 64x64 alpha map.
};

struct Mcal
{
	vector<McalEntry> alphaMap;
};

/*struct Mclq // Deprecated with WotLK, use MH2O
{

};*/

/*struct Mcse // No fucking idea, documentation too poor
{

};*/

class ADT_MCNK
{
	public:
		ADT_MCNK(ifstream &adtFile);
		~ADT_MCNK();

		float GetValNoLOD(int x, int y);
		float GetValLOD(int x, int y);
		Vector3f GetPosition();

	private:
		McnkHeader m_header;
		Mcvt       m_mcvt;
		Mcnr       m_mcnr;
		Mcly       m_mcly;
		Mcrf       m_mcrf;
		Mcsh       m_mcsh;
		Mcal       m_mcal;

		int GetIdNoLOD(int x, int y);
		int GetIdLOD(int x, int y);
};