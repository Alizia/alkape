#pragma once

#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>

#include "wdtStruct.h"

using namespace std;

class WDT
{
	public:
		WDT(string nameFile);
		~WDT();
		
		::uint32_t GetMainWidthMap();
		::uint32_t GetMainFlags(int x, int y);

	private:
		WDT_MVER* m_mver;
		WDT_MPHD* m_mphd;
		WDT_MAIN* m_main;
		WDT_MWMO* m_mwmo;
		WDT_MODF* m_modf;
};