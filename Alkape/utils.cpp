#include "utils.h"

void StrReplace(string &str, const string &from, const string &to)
{
	size_t start_pos = str.find(from);
	if (!(start_pos == string::npos))
	{
		str.replace(start_pos, from.length(), to);
	}
}

float Dist2Points(Vector2f a, Vector2f b)
{
	float result;
	float diffx = b.x - a.x;
	float diffy = b.y - a.y;

	result = sqrt(diffx*diffx + diffy*diffy);
	
	return result;
}
