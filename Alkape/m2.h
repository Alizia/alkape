#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>

#include "m2Struct.h"
#include "shader.h"
#include "ressourcesManager.h"

// Macro utile au VBO
#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif

using namespace std;
using namespace sf;
using namespace glm;

class M2Data
{
	public:
		M2Data(string nameFile, float scale, string const vertexShader, string const fragmentShader);
		~M2Data();

		void Load();
		void Show(mat4 &projection, mat4 &modelview, RessourcesManager &RManager);

		void ExtractData();
		void CreateOrderedVertices();

		string GetPathTexture();

	private:
		string m_nameFile;
		ShaderTuto m_shader;
		GLuint m_vboID;
		GLuint m_vaoID;

		float*  m_orderedVertices;
		float* m_orderedTexturesCoords;
		::int32_t m_verticesSize;
		::int32_t m_textureCoordsSize;


		// Header M2
		  char     m_m2Magic[4];             // MD20
		::uint32_t m_version;              // 0x80100000 (first digit of the build the format was last updated)
		::uint32_t m_lName;                // Length of the model's name
		::uint32_t m_ofsName;              // Offset to the name
		::uint32_t m_globalModelFlags;     // (0,1,3 seen), connected to field 4 of CreatureModelData.dbc?
		::uint32_t m_nGlobalModelFlags;
		::uint32_t m_ofsGlobalModelFlags;  // A list of timestamps
		::uint32_t m_nAnimations;
		::uint32_t m_ofsAnimations;        // Information about the animations in the model
		::uint32_t m_nAnimationLookup;
		::uint32_t m_ofsAnimationLookup;   // Mapping of global IDs to the entries in the Animation sequences block
		::uint32_t m_nBones;
		::uint32_t m_ofsBones;             // Information about the bones in this model
		::uint32_t m_nKeyBoneLookup;
		::uint32_t m_ofsKeyBoneLookup;     // Lookup table for key skeletal bones
		::uint32_t m_nVertices;
		::uint32_t m_ofsVertices;          // Vertices of the model
		::uint32_t m_nViews;               // Views (LOD) are now in .skins
		::uint32_t m_nColors;
		::uint32_t m_ofsColors;            // Color definitions
		::uint32_t m_nTextures;
		::uint32_t m_ofsTextures;          // Textures of this model. Now .skin ?
		::uint32_t m_nTransparency;
		::uint32_t m_ofsTransparency;      // Transparency of textures
		::uint32_t m_nTextureAnimations;
		::uint32_t m_ofsTextureAnimations;
		::uint32_t m_nTexReplace;
		::uint32_t m_ofsTexReplace;        // Replaceable Textures
		::uint32_t m_nRenderFlags;
		::uint32_t m_ofsRenderFlags;       // Blending modes / render flags
		::uint32_t m_nBoneLookupTable;
		::uint32_t m_ofsBoneLookupTable;   // A bone lookup table
		::uint32_t m_nTexLookup;
		::uint32_t m_ofsTexLookup;         // The same for textures
		::uint32_t m_nTexUnits;
		::uint32_t m_ofsTexUnits;          // And texture units. Somewhere they have to be too.
		::uint32_t m_nTransLookup;
		::uint32_t m_ofsTransLookup;       // Everything needs its lookup. Here are the transparencies.
		::uint32_t m_nTexAnimLookup;
		::uint32_t m_ofsTexAnimLookup;     // Wait. Do we have animated Textures ? Wasn't ofsTexAnims deleted ?
		  float    m_theFloats[14];        // Noone knows. Meeh, they are here.
		::uint32_t m_nBoundingTriangles;
		::uint32_t m_ofsBoundingTriangles; // Our bounding volumes. Similar structure like in the old ofsViews.
		::uint32_t m_nBoundingVertices;
		::uint32_t m_ofsBoundingVertices;
		::uint32_t m_nBoundingNormals;
		::uint32_t m_ofsBoundingNormals;
		::uint32_t m_nAttachments; 
		::uint32_t m_ofsAttachments;       // Attachments are for weapons etc.
		::uint32_t m_nAttachLookup;
		::uint32_t m_ofsAttachLookup;      // Of course with a lookup
		::uint32_t m_nAttachments_2;
		::uint32_t m_ofsAttachments_2;     // And some second block
		::uint32_t m_nLights;
		::uint32_t m_ofsLights;            // Lights are mainly used in loginscreens but in wands and some doodads too.
		::uint32_t m_nCameras;
		::uint32_t m_ofsCameras;           // The cameras are present in most models for having a model in the Character-Tab
		::uint32_t m_nCameraLookup;
		::uint32_t m_ofsCameraLookup;      // And lookup-time again
		::uint32_t m_nRibbonEmitters;
		::uint32_t m_ofsRibbonEmitters;    // Things swirling around. See the CoT-entrance for light-trails
		::uint32_t m_nParticleEmitters;
		::uint32_t m_ofsParticleEmitters;  // Spells and weapons, doodads and loginscreens use them. Blood dripping of a blade ? Particles.
		// Fin Header M2

		// Header Skin
		  char	   m_skinMagic[4];
		::uint32_t m_nIndices;
		::uint32_t m_ofsIndices;
		::uint32_t m_nTriangles;
		::uint32_t m_ofsTriangles;
		::uint32_t m_nProperties;
		::uint32_t m_ofsProperties;
		::uint32_t m_nSubmeshes;
		::uint32_t m_ofsSubmeshes;
		::uint32_t m_nTextureUnits;
		::uint32_t m_ofsTextureUnits;
		::uint32_t m_LOD;
		// Fin Header Skin


		// Data m2
		Vertices* m_vertices;
		Textures* m_textures;
		// Fin Data m2

		// Data Skin
		Indices* m_indices;
		::Triangles* m_triangles;
		VertexProperties* m_vertexProperties;
		Submeshes* m_submeshes;
		TextureUnits* m_textureUnits;
		// Fin Data Skin

		void ReadHeader();
		void ReadData();
};

