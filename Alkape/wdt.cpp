#include "wdt.h"

WDT::WDT(string nameFile)
{
	ifstream wdtFile(nameFile, ios::in | ios::binary);

	wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // MVER
	m_mver = new WDT_MVER(wdtFile);

	wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // MPHD
	m_mphd = new WDT_MPHD(wdtFile);

	wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // MAIN
	m_main = new WDT_MAIN(wdtFile);

	wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // MWMO
	m_mwmo = new WDT_MWMO(wdtFile);

	if (m_mwmo->GetSize() != 0)
	{
		wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // MODF
		m_modf = new WDT_MODF(wdtFile);
	}

	wdtFile.close();

	/*for (int i = 0; i < m_main->GetWidthMap(); i++)
	{
		for (int j = 0; j < m_main->GetWidthMap(); j++)
		{
			if (m_main->GetFlags(i, j) == 1)
			{
				cout << "adt : " << i << " " << j << endl;
			}
		}
	}*/
}

WDT::~WDT()
{
}

::uint32_t WDT::GetMainWidthMap()
{
	return m_main->GetWidthMap();
}

::uint32_t WDT::GetMainFlags(int x, int y)
{
	return m_main->GetFlags(x, y);
}
