#pragma once
#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <SFML\System.hpp>

#include "utils.h"


using namespace std;
using namespace sf;

// M2

class Vertices
{
	public:
		Vertices(string nameFile, ::uint32_t nVertices, ::uint32_t ofsVertices);
		~Vertices();

		float* m_position;        // A vector to the position of the vertex
		::uint8_t* m_boneWeight;  // The vertex weight for 4 bones
		::uint8_t* m_boneIndices; // Which are referenced here
		float* m_normal;          // A normal vector
		float* m_textureCoords;   // Coordinates for a texture
		float* m_unknown;         // Null ?

	private:

};

struct m2texture
{
	::uint32_t type;
	::uint32_t flags;
	::uint32_t lenFilename;
	::uint32_t ofsFilename;
};

struct texture
{
	::uint32_t type;
	::uint32_t flags;
	string filename;
};

class Textures
{
	public:
		Textures(string nameFile, ::uint32_t nTextures, ::uint32_t ofsTextures);
		~Textures();

		vector<texture> m_texturesList;

	private:
		void ReadString(string &string, ifstream &stream, ::uint32_t offset, ::uint32_t length);
};


// SKIN

class Indices
{
	public:
		Indices(string nameFile, ::uint32_t nIndices, ::uint32_t ofsIndices);
		~Indices();

		vector<::int16_t> m_indexVertices;
	private:

};

struct Triangle
{
	::int16_t i1;
	::int16_t i2;
	::int16_t i3;
};

class Triangles
{
	public:
		Triangles(string nameFile, ::uint32_t nTriangles, ::uint32_t ofsTriangles);
		~Triangles();

		vector<Triangle> m_trianglesList;
	private:

};

struct vertexProperty
{
	::uint8_t boneIndice1;
	::uint8_t boneIndice2;
	::uint8_t boneIndice3;
	::uint8_t boneIndice4;
};

class VertexProperties
{
	public:
		VertexProperties(string nameFile, ::uint32_t nProperties, ::uint32_t ofsProperties);
		~VertexProperties();

		vector<vertexProperty> m_propritiesList;
	private:

};

struct Submesh
{
	::uint32_t id;
	::uint16_t startVertex;
	::uint16_t nVertices;
	::uint16_t startTriangle;
	::uint16_t nTriangles;
	::uint16_t nBones;
	::uint16_t startBone;
	::uint16_t unknown;
	::uint16_t rootBone;
	Vector3f centerMass;
	Vector3f centerBoundingBox;
	float radius;
};

class Submeshes
{
	public:
		Submeshes(string nameFile, ::uint32_t nSubmeshes, ::uint32_t ofsSubmeshes);
		~Submeshes();

		vector<Submesh> m_submeshesList;
	private:

};

struct TextureUnit
{
	::uint16_t flags;
	::uint16_t shading;
	::uint16_t submeshIndex;
	::uint16_t submeshIndex2;
	::int16_t colorIndex;
	::uint16_t renderFlag;
	::uint16_t texUnitNumber;
	::uint16_t mode;
	::uint16_t texture;
	::uint16_t texUnitNumber2;
	::uint16_t transparency;
	::uint16_t textureAnim;
};

class TextureUnits
{
	public:
		TextureUnits(string nameFile, ::uint32_t nTextureUnits, ::uint32_t ofsTextureUnits);
		~TextureUnits();

		vector<TextureUnit> m_textureUnitsList;
	private:

};