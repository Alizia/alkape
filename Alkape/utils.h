#pragma once

#include <string>
#include <iostream>
#include <stdint.h>
#include <math.h>

#include <SFML\System.hpp>

using namespace std;
using namespace sf;

void StrReplace(string &str, const string &from, const string &to);

float Dist2Points(Vector2f a, Vector2f b);