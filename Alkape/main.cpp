#include <SFML/Window.hpp>
#include <cstdlib>
#include "sceneOpenGL.h"
#include <fstream>
#include <sstream>
#include <iostream>

using namespace sf;
using namespace std;

int main()
{
	SceneOpenGL scene("Alkape", 1080, 720);

	if (!scene.InitWindow())
	{
		cout << "Error : Window initialisation failed !" << endl;
		return -1;
	}

	if (!scene.InitGL())
	{
		cout << "Error : GL initialisation failed !" << endl;
		return -1;
	}

	scene.MainLoop();

	return 0;
}