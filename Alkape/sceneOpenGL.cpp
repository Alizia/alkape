#include "sceneOpenGL.h"

SceneOpenGL::SceneOpenGL(string titleWindow, int widthWindow, int heightWindow)
{
	m_titleWindow = titleWindow;
	m_widthWindow = widthWindow;
	m_heightWindow = heightWindow;
}

SceneOpenGL::~SceneOpenGL()
{

}

bool SceneOpenGL::InitWindow()
{
	m_window.create(VideoMode(m_widthWindow, m_heightWindow), m_titleWindow, Style::Default, ContextSettings(32));
	m_window.setFramerateLimit(60);

	m_window.setMouseCursorVisible(false);
	m_window.setMouseCursorGrabbed(true);

	return true;
}

bool SceneOpenGL::InitGL()
{
	// Initialisation GLEW
	GLenum initGLEW(glewInit());
	if (initGLEW != GLEW_OK)
	{
		cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initGLEW) << endl;
		system("PAUSE");
		return false;
	}

	glEnable(GL_DEPTH_TEST);

	return true;
}

void SceneOpenGL::MainLoop()
{
	
	//FreeFlyCamera* camera;
	m_camera = new FreeFlyCamera(vec3(4800.000000f, 1066.666016f, 13.137494f));

	//Binds
	KeyConf keyconf;
	KeyStates keystates;
	InitKeyConf(keystates, keyconf);

	Uint32 lastTime = GetTickCount();
	Uint32 newTime = lastTime;
	Uint32 diff = 0;

	Vector2i centerPos(m_window.getSize().x / 2, m_window.getSize().y / 2);

	// Ressources loading, OpenGL Init
	RessourcesManager RManager;

	mat4 projection = perspective(radians(70.0f), (float)m_widthWindow/m_heightWindow, 1.0f, 100000.0f);
	mat4 modelview = mat4(1);

	LoadMap("Kalimdor");

	addTexture("TILESET/EmeraldDreamCanyon/DreamCGrass02.png", RManager, true); // Textures adding in Ressources Manager

	bool running = true;
	while (running)
	{
		// Event management
		while (m_window.pollEvent(m_event))
		{
			if (m_event.type == sf::Event::Closed)
			{
				// Program exit
				running = false;
			}
			else if (m_event.type == sf::Event::Resized)
			{
				// Resize viewport when window resized
				glViewport(0, 0, m_event.size.width, m_event.size.height);
			}
			else if (m_event.type == Event::MouseMoved)
			{
				m_camera->OnMouseMotion(m_event, centerPos, m_window);
			}
			else if (m_event.type == Event::KeyPressed || m_event.type == Event::KeyReleased)
			{
				m_camera->OnKeyboard(m_event, keystates);
			}
		}

		newTime = GetTickCount();
		diff = newTime - lastTime;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_camera->Animate(diff, keystates, keyconf);

		// Modelview matrix init
		modelview = mat4(1.0);
		
		m_camera->Look(modelview);

		ShowMap(projection, modelview, RManager);

		lastTime = GetTickCount();

		// Window render
		m_window.display();
	}

	// Free ressources
	delete m_camera;
}

void SceneOpenGL::LoadMap(string mapName)
{
	WDT wdt("mpq/world/Maps/"+ mapName + "/"+ mapName + ".wdt");

	::uint32_t width = wdt.GetMainWidthMap();

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (wdt.GetMainFlags(i, j) == 1)
			{
				cout << "Map loading " << i << " " << j << endl;
				ADT adt("mpq/world/Maps/" + mapName + "/" + mapName + "_" + to_string(i) + "_" + to_string(j) + ".adt", 1, "Shaders/texture.vert", "Shaders/texture.frag");

				m_maps.push_back(adt);
				cout << "Map pushed " << i << " " << j << endl;

			}
		}
	}

	// Loading adt
	for (int i = 0; i < m_maps.size(); i++)
	{
		m_maps[i].Load();
		cout << "Map loaded : " << m_maps[i].m_nameFile << endl;
	}
	cout << "All maps loaded." << endl;
}

void SceneOpenGL::ShowMap(mat4 & projection, mat4 & modelview, RessourcesManager & RManager)
{
	vec3 camPos = m_camera->GetPosition();
	Vector2f a(camPos.x, camPos.y);

	//cout << "Bite 1 camPos = " << camPos.x << " " << camPos.y << " " << camPos.z << endl;
	//system("PAUSE");

	for (int i = 0; i < m_maps.size(); i++)
	{
		Vector3f adtMiddlePos = m_maps[i].GetMcnkPos(119);
		//cout << "Bite 2" << endl;
		Vector2f b(adtMiddlePos.x, adtMiddlePos.y);
		//cout << "Bite 3 b = " << b.x << " " << b.y << endl;
		//system("PAUSE");
		if (Dist2Points(a, b) < 1000)
		{
			//cout << "Bite 4 adtMiddlePos = " << adtMiddlePos.x << " " << adtMiddlePos.y << " " << adtMiddlePos.z << endl;
			m_maps[i].Show(projection, modelview, RManager);
		}
	}
}

