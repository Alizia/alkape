#include "ressourcesManager.h"

void RessourcesManager::LoadTexture(string name, Texture tex)
{
		this->_textures[name] = tex;
}

Texture &RessourcesManager::GetTexture(string name)
{
	return this->_textures.at(name);
}