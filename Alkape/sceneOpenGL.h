#pragma once

#include <string>
#include <GL\glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#include "shader.h"
#include "ressourcesManager.h"
#include "camera.h"
#include "cube.h"
#include "textures.h"
#include "m2.h"
#include "wdt.h"
#include "adt.h"
#include "utils.h"


using namespace sf;
using namespace std;
using namespace glm;

class SceneOpenGL
{
	public:

		SceneOpenGL(string titleWindow, int widthWindow, int heightWindow);
		~SceneOpenGL();

		bool InitWindow();
		bool InitGL();
		void MainLoop();
		void LoadMap(string mapPath);
		void ShowMap(mat4 & projection, mat4 & modelview, RessourcesManager & RManager);

	private:

		string m_titleWindow;
		int m_widthWindow;
		int m_heightWindow;

		Window m_window;
		Event m_event;

		FreeFlyCamera* m_camera;

		vector<ADT> m_maps;
};