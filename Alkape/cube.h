#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#include <cstring>

//#include <SFML/Window.hpp>

#include "shader.h"
#include "ressourcesManager.h"

// Macro utile au VBO
#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif

using namespace std;
using namespace sf;
using namespace glm;

class Cube
{
	public:
		Cube(float taille, string const vertexShader, string const fragmentShader);
		~Cube();

		void Show(mat4 &projection, mat4 &modelview);
		void Load();
		void updateVBO(void *donnees, int tailleBytes, int decalage);

	protected:
		ShaderTuto m_shader;
		float m_vertices[108];
		float m_couleurs[108];

		GLuint m_vboID;
		GLuint m_vaoID;
		int m_tailleVerticesBytes;
		int m_tailleCouleursBytes;
};

class Caisse : public Cube
{
	public:
		Caisse(float taille, string const vertexShader, string const fragmentShader, string const strTexture);
		~Caisse();

		void Show(mat4 &projection, mat4 &modelview, RessourcesManager &RManager);
		void Load();

	private:
		string m_strTexture;
		float m_coordTexture[72];
		int m_tailleCoordTextureBytes;
};