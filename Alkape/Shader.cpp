#include "Shader.h"

using namespace std;

// Constructors et Destructor
ShaderTuto::ShaderTuto() : m_vertexID(0), m_fragmentID(0), m_programID(0), m_vertexSource(), m_fragmentSource()
{

}


ShaderTuto::ShaderTuto(ShaderTuto const &shaderACopier)
{
    // Source file copy
    m_vertexSource = shaderACopier.m_vertexSource;
    m_fragmentSource = shaderACopier.m_fragmentSource;


    // Load new shader
    charger();
}


ShaderTuto::ShaderTuto(std::string vertexSource, std::string fragmentSource) : m_vertexID(0), m_fragmentID(0), m_programID(0), m_vertexSource(vertexSource), m_fragmentSource(fragmentSource)
{

}


ShaderTuto::~ShaderTuto()
{
    // Shader destruction
    glDeleteShader(m_vertexID);
    glDeleteShader(m_fragmentID);
    glDeleteProgram(m_programID);
}


// Methods
ShaderTuto& ShaderTuto::operator=(ShaderTuto const &shaderACopier)
{
	// Source file copy
    m_vertexSource = shaderACopier.m_vertexSource;
    m_fragmentSource = shaderACopier.m_fragmentSource;


	//cout << "BITE !" << endl;
	// Load new shader
    charger();


    // Return pointer this
    return *this;
}


bool ShaderTuto::charger()
{
    // Potential old shader destruction
    if(glIsShader(m_vertexID) == GL_TRUE)
        glDeleteShader(m_vertexID);

    if(glIsShader(m_fragmentID) == GL_TRUE)
        glDeleteShader(m_fragmentID);

    if(glIsProgram(m_programID) == GL_TRUE)
        glDeleteProgram(m_programID);


    // Shader compilation
    if(!compilerShader(m_vertexID, GL_VERTEX_SHADER, m_vertexSource))
        return false;

    if(!compilerShader(m_fragmentID, GL_FRAGMENT_SHADER, m_fragmentSource))
        return false;


    // Create program
    m_programID = glCreateProgram();

    // Shader attach
    glAttachShader(m_programID, m_vertexID);
    glAttachShader(m_programID, m_fragmentID);


	// Lock shader entry
    glBindAttribLocation(m_programID, 0, "in_Vertex");
    glBindAttribLocation(m_programID, 1, "in_Color");
    glBindAttribLocation(m_programID, 2, "in_TexCoord0");


    // Program link
    glLinkProgram(m_programID);


    // Link check
    GLint erreurLink(0);
    glGetProgramiv(m_programID, GL_LINK_STATUS, &erreurLink);


    // If errors
    if(erreurLink != GL_TRUE)
    {
        // Get size error
        GLint tailleErreur(0);
        glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &tailleErreur);


        // Memory allocation
        char *erreur = new char[tailleErreur + 1];


        // Get error
        glGetShaderInfoLog(m_programID, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';


        // Display error
        std::cout << erreur << std::endl;


		// Free memory and return false
        delete[] erreur;
        glDeleteProgram(m_programID);

        return false;
    }
    else
        return true;
}


bool ShaderTuto::compilerShader(GLuint &shader, GLenum type, std::string const &fichierSource)
{
    // Shader creation
    shader = glCreateShader(type);


    // Check shader
    if(shader == 0)
    {
        std::cout << "Error, shader type (" << type << ") don't exist" << std::endl;
        return false;
    }


    // Read stream
    std::ifstream fichier(fichierSource.c_str());


    // Open test
    if(!fichier)
    {
        std::cout << "Erreur le fichier " << fichierSource << " est introuvable" << std::endl;
        glDeleteShader(shader);

        return false;
    }


    std::string ligne;
    std::string codeSource;


    // Reading
    while(getline(fichier, ligne))
        codeSource += ligne + '\n';


    // Close file
    fichier.close();


    // Get C source code
    const GLchar* chaineCodeSource = codeSource.c_str();


    // Send source code to shader
    glShaderSource(shader, 1, &chaineCodeSource, 0);


    // Shader compilation
    glCompileShader(shader);


    // Check compilation
    GLint erreurCompilation(0);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &erreurCompilation);


    // If error
    if(erreurCompilation != GL_TRUE)
    {
        // Get error size
        GLint tailleErreur(0);
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &tailleErreur);


        // Memory allocation
        char *erreur = new char[tailleErreur + 1];


        // Get error
        glGetShaderInfoLog(shader, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';


        // Display error
        std::cout << erreur << std::endl;


		// Free memory and return false
        delete[] erreur;
        glDeleteShader(shader);

        return false;
    }
	else
        return true;
}

GLuint ShaderTuto::GetVertexID() const
{
	return m_vertexID;
}

GLuint ShaderTuto::GetFragmentID() const
{
	return m_fragmentID;
}

GLuint ShaderTuto::GetProgramID() const
{
    return m_programID;
}