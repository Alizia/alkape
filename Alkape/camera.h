#pragma once
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
//#include <GL/glu.h>
#define _USE_MATH_DEFINES
#include <math.h>
//#include "vector3d.h"
#include "keybinds.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

using namespace sf;
using namespace std;
using namespace glm;

class FreeFlyCamera
{
	public:
		FreeFlyCamera(const vec3 & position = vec3(0.0, 0.0, 0.0));

		virtual void OnMouseMotion(Event &event, Vector2i &oldLocalPosition, Window &window);
		virtual void OnKeyboard(Event &event, KeyStates &keystates);

		virtual void Animate(Uint32 diff, KeyStates &keystates, KeyConf &keyconf);

		virtual void SetSpeed(float speed);
		virtual void SetSensivity(float sensivity);
		virtual void SetPosition(const vec3 & position);

		virtual vec3 GetPosition();

		virtual void Look(mat4 &modelview);

		virtual ~FreeFlyCamera();

	protected:
		float speed;
		float sensivity;
		vec3 position;
		vec3 target;
		vec3 forward;
		vec3 left;
		float theta;
		float phi;

		void VectorsFromAngles();
};