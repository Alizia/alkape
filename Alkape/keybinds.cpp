#include "keybinds.h"

void InitKeyConf(KeyStates &keystates, KeyConf &keyconf)
{
	keyconf["forward"]      = Keyboard::Key::Z;
	keyconf["backward"]     = Keyboard::Key::S;
	keyconf["strafe_left"]  = Keyboard::Key::Q;
	keyconf["strafe_right"] = Keyboard::Key::D;
	keyconf["up"]           = Keyboard::Key::Space;
	keyconf["down"]         = Keyboard::Key::C;
	keyconf["boost"]        = Keyboard::Key::LShift;

	KeyConf::iterator it;
	for (it = keyconf.begin(); it != keyconf.end(); it++)
	{
		keystates[keyconf[it->first]] = false;
	}
}