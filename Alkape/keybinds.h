#pragma once

#include <map>
#include <SFML/Window.hpp>

using namespace sf;
using namespace std;

typedef map<Keyboard::Key, bool> KeyStates;
typedef map<std::string, Keyboard::Key> KeyConf;

void InitKeyConf(KeyStates &keystates, KeyConf &keyconf);