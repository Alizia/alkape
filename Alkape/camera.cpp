#include "camera.h"

// FreeFly Cam
FreeFlyCamera::FreeFlyCamera(const vec3 & position)
{
	this->position = position;
	phi = 0;
	theta = 0;
	VectorsFromAngles();

	speed = 0.01f;
	sensivity = 0.2f;
}

void FreeFlyCamera::OnMouseMotion(Event &event, Vector2i &centerPos, Window &window)
{
	// Calculate elocity 
	Vector2i deltaPos;
	Vector2i newLocalPosition = Mouse::getPosition(window);

	deltaPos = newLocalPosition - centerPos;

	theta -= deltaPos.x * sensivity;
	phi -= deltaPos.y * sensivity; 

	Mouse::setPosition(centerPos, window);

	VectorsFromAngles();
}

void FreeFlyCamera::VectorsFromAngles()
{
	static const vec3 up(0.0, 0.0, 1.0);

	if (phi > 89)
	{
		phi = 89;
	}
	else if (phi < -89)
	{
		phi = -89;
	}

	float r_temp = cos(phi * M_PI / 180);
	forward.z = sin(phi * M_PI / 180);
	forward.x = r_temp * cos(theta * M_PI / 180);
	forward.y = r_temp * sin(theta * M_PI / 180);

	left = normalize(cross(up, forward));

	target = position + forward;
}

void FreeFlyCamera::OnKeyboard(Event &event, KeyStates &keystates)
{
	for (KeyStates::iterator it = keystates.begin(); it != keystates.end();
		it++)
	{
		if (event.key.code == it->first)
		{
			it->second = (event.type == Event::KeyPressed); //true if pressed
			break;
		}
	}
}

void FreeFlyCamera::Animate(Uint32 diff, KeyStates &keystates, KeyConf &keyconf)
{
	float realspeed = (keystates[keyconf["boost"]])?10*speed:speed;

	float dist = (realspeed * diff);
	vec3 vForward = forward * dist;
	vec3 vLeft = left * dist;


	if (keystates[keyconf["forward"]])
	{
		position += vForward;
	}
	if (keystates[keyconf["backward"]])
	{
		position -= vForward;
	}
	if (keystates[keyconf["strafe_left"]])
	{
		position += vLeft;
	}
	if (keystates[keyconf["strafe_right"]])
	{
		position -= vLeft;
	}
	if (keystates[keyconf["up"]])
	{
		position += vec3(0.0, 0.0, dist);
	}
	if (keystates[keyconf["down"]])
	{
		position += vec3(0.0, 0.0, 0-dist);
	}

	target = position + forward;
}

void FreeFlyCamera::Look(mat4 &modelview)
{
	modelview = lookAt(position, target, vec3(0.0,0.0,1.0));
}

void FreeFlyCamera::SetSpeed(float speed)
{
	this->speed = speed;
}

void FreeFlyCamera::SetSensivity(float sensivity)
{
	this->sensivity = sensivity;
}

void FreeFlyCamera::SetPosition(const vec3 & position)
{
	this->position = position;
}

vec3 FreeFlyCamera::GetPosition()
{
	return this->position;
}

FreeFlyCamera::~FreeFlyCamera()
{

}