#include "m2.h"

M2Data::M2Data(string nameFile, float scale, string const vertexShader, string const fragmentShader)
{
	m_shader = ShaderTuto(vertexShader, fragmentShader);
	m_shader.charger();

	m_vboID = 0;
	m_vaoID = 0;
	
	m_nameFile = nameFile;

	ExtractData(); // Header and data reading
}

M2Data::~M2Data()
{
	delete[] m_vertices;
	delete[] m_orderedVertices;
	delete[] m_orderedTexturesCoords;

	// VBO Destruction 
	glDeleteBuffers(1, &m_vboID);

	// VAO Destruction
	glDeleteVertexArrays(1, &m_vaoID);
}

void M2Data::Load()
{
	// Destruction of a potential old VBO
	if (glIsBuffer(m_vboID) == GL_TRUE)
		glDeleteBuffers(1, &m_vboID);


	// ID generation
	glGenBuffers(1, &m_vboID);


	// Lock VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID);


	// Allocation video memory
	glBufferData(GL_ARRAY_BUFFER, m_verticesSize + m_textureCoordsSize, 0, GL_STATIC_DRAW);


	// Data transfer
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_verticesSize, m_orderedVertices);
	glBufferSubData(GL_ARRAY_BUFFER, m_verticesSize, m_textureCoordsSize, m_orderedTexturesCoords);


	// Unlock VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void M2Data::Show(mat4 &projection, mat4 &modelview, RessourcesManager &RManager)
{
	// Shader activation
	glUseProgram(m_shader.GetProgramID());


		// Lock VBO
		glBindBuffer(GL_ARRAY_BUFFER, m_vboID);


			// Access to vertices in video memory
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
			glEnableVertexAttribArray(0);


			// Send textures coords
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, 0, BUFFER_OFFSET(m_verticesSize));
			glEnableVertexAttribArray(2);


		// Unlock VBO
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// Send matrices
		glUniformMatrix4fv(glGetUniformLocation(m_shader.GetProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(m_shader.GetProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));


		// Lock texture (the first one)
		Texture::bind(&RManager.GetTexture(m_textures->m_texturesList[0].filename)); // Replace the OpenGL's glBindTexture function

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // To draw a surface with outlined polygons 

		// Render
		glDrawArrays(GL_TRIANGLES, 0, m_nTriangles);

		// Unlock texture
		Texture::bind(0); // Replace the OpenGL's glBindTexture function
		
		// Array deactivation
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(0);


	// Shader deactivation
	glUseProgram(0);
}

void M2Data::ExtractData()
{
	ReadHeader();
	ReadData();

	CreateOrderedVertices();
}

void M2Data::ReadHeader()
{
	// M2 Header
	ifstream m2File(m_nameFile + ".m2", ios::in | ios::binary);

	m2File.read((char*)&m_m2Magic, sizeof(m_m2Magic));
	m2File.read((char*)&m_version, sizeof(m_version));
	m2File.read((char*)&m_lName, sizeof(m_lName));
	m2File.read((char*)&m_ofsName, sizeof(m_ofsName));
	m2File.read((char*)&m_globalModelFlags, sizeof(m_globalModelFlags));
	m2File.read((char*)&m_nGlobalModelFlags, sizeof(m_nGlobalModelFlags));
	m2File.read((char*)&m_ofsGlobalModelFlags, sizeof(m_ofsGlobalModelFlags));
	m2File.read((char*)&m_nAnimations, sizeof(m_nAnimations));
	m2File.read((char*)&m_ofsAnimations, sizeof(m_ofsAnimations));
	m2File.read((char*)&m_nAnimationLookup, sizeof(m_nAnimationLookup));
	m2File.read((char*)&m_ofsAnimationLookup, sizeof(m_ofsAnimationLookup));
	m2File.read((char*)&m_nBones, sizeof(m_nBones));
	m2File.read((char*)&m_ofsBones, sizeof(m_ofsBones));
	m2File.read((char*)&m_nKeyBoneLookup, sizeof(m_nKeyBoneLookup));
	m2File.read((char*)&m_ofsKeyBoneLookup, sizeof(m_ofsKeyBoneLookup));
	m2File.read((char*)&m_nVertices, sizeof(m_nVertices));
	m2File.read((char*)&m_ofsVertices, sizeof(m_ofsVertices));
	m2File.read((char*)&m_nViews, sizeof(m_nViews));
	m2File.read((char*)&m_nColors, sizeof(m_nColors));
	m2File.read((char*)&m_ofsColors, sizeof(m_ofsColors));
	m2File.read((char*)&m_nTextures, sizeof(m_nTextures));
	m2File.read((char*)&m_ofsTextures, sizeof(m_ofsTextures));
	m2File.read((char*)&m_nTransparency, sizeof(m_nTransparency));
	m2File.read((char*)&m_ofsTransparency, sizeof(m_ofsTransparency));
	m2File.read((char*)&m_nTextureAnimations, sizeof(m_nTextureAnimations));
	m2File.read((char*)&m_ofsTextureAnimations, sizeof(m_ofsTextureAnimations));
	m2File.read((char*)&m_nTexReplace, sizeof(m_nTexReplace));
	m2File.read((char*)&m_ofsTexReplace, sizeof(m_ofsTexReplace));
	m2File.read((char*)&m_nRenderFlags, sizeof(m_nRenderFlags));
	m2File.read((char*)&m_ofsRenderFlags, sizeof(m_ofsRenderFlags));
	m2File.read((char*)&m_nBoneLookupTable, sizeof(m_nBoneLookupTable));
	m2File.read((char*)&m_ofsBoneLookupTable, sizeof(m_ofsBoneLookupTable));
	m2File.read((char*)&m_nTexLookup, sizeof(m_nTexLookup));
	m2File.read((char*)&m_ofsTexLookup, sizeof(m_ofsTexLookup));
	m2File.read((char*)&m_nTexUnits, sizeof(m_nTexUnits));
	m2File.read((char*)&m_ofsTexUnits, sizeof(m_ofsTexUnits));
	m2File.read((char*)&m_nTransLookup, sizeof(m_nTransLookup));
	m2File.read((char*)&m_ofsTransLookup, sizeof(m_ofsTransLookup));
	m2File.read((char*)&m_nTexAnimLookup, sizeof(m_nTexAnimLookup));
	m2File.read((char*)&m_ofsTexAnimLookup, sizeof(m_ofsTexAnimLookup));
	m2File.read((char*)&m_theFloats, sizeof(m_theFloats));
	m2File.read((char*)&m_nBoundingTriangles, sizeof(m_nBoundingTriangles));
	m2File.read((char*)&m_ofsBoundingTriangles, sizeof(m_ofsBoundingTriangles));
	m2File.read((char*)&m_nBoundingVertices, sizeof(m_nBoundingVertices));
	m2File.read((char*)&m_ofsBoundingVertices, sizeof(m_ofsBoundingVertices));
	m2File.read((char*)&m_nBoundingNormals, sizeof(m_nBoundingNormals));
	m2File.read((char*)&m_ofsBoundingNormals, sizeof(m_ofsBoundingNormals));
	m2File.read((char*)&m_nAttachments, sizeof(m_nAttachments));
	m2File.read((char*)&m_ofsAttachments, sizeof(m_ofsAttachments));
	m2File.read((char*)&m_nAttachLookup, sizeof(m_nAttachLookup));
	m2File.read((char*)&m_ofsAttachLookup, sizeof(m_ofsAttachLookup));
	m2File.read((char*)&m_nAttachments_2, sizeof(m_nAttachments_2));
	m2File.read((char*)&m_ofsAttachments_2, sizeof(m_ofsAttachments_2));
	m2File.read((char*)&m_nLights, sizeof(m_nLights));
	m2File.read((char*)&m_ofsLights, sizeof(m_ofsLights));
	m2File.read((char*)&m_nCameras, sizeof(m_nCameras));
	m2File.read((char*)&m_ofsCameras, sizeof(m_ofsCameras));
	m2File.read((char*)&m_nCameraLookup, sizeof(m_nCameraLookup));
	m2File.read((char*)&m_ofsCameraLookup, sizeof(m_ofsCameraLookup));
	m2File.read((char*)&m_nRibbonEmitters, sizeof(m_nRibbonEmitters));
	m2File.read((char*)&m_ofsRibbonEmitters, sizeof(m_ofsRibbonEmitters));
	m2File.read((char*)&m_nParticleEmitters, sizeof(m_nParticleEmitters));
	m2File.read((char*)&m_ofsParticleEmitters, sizeof(m_ofsParticleEmitters));

	m2File.close();


	// SKIN Header
	ifstream skinFile(m_nameFile + "00.skin", ios::in | ios::binary);

	skinFile.read((char*)&m_skinMagic, sizeof(m_skinMagic));
	skinFile.read((char*)&m_nIndices, sizeof(m_nIndices));
	skinFile.read((char*)&m_ofsIndices, sizeof(m_ofsIndices));
	skinFile.read((char*)&m_nTriangles, sizeof(m_nTriangles));
	skinFile.read((char*)&m_ofsTriangles, sizeof(m_ofsTriangles));
	skinFile.read((char*)&m_nProperties, sizeof(m_nProperties));
	skinFile.read((char*)&m_ofsProperties, sizeof(m_ofsProperties));
	skinFile.read((char*)&m_nSubmeshes, sizeof(m_nSubmeshes));
	skinFile.read((char*)&m_ofsSubmeshes, sizeof(m_ofsSubmeshes));
	skinFile.read((char*)&m_nTextureUnits, sizeof(m_nTextureUnits));
	skinFile.read((char*)&m_ofsTextureUnits, sizeof(m_ofsTextureUnits));
	skinFile.read((char*)&m_LOD, sizeof(m_LOD));

	skinFile.close();
}

void M2Data::ReadData()
{
	// Data M2
	m_vertices = new Vertices(m_nameFile+".m2", m_nVertices, m_ofsVertices);
	m_textures = new Textures(m_nameFile+".m2", m_nTextures, m_ofsTextures);

	// Data SKIN
	m_indices = new Indices(m_nameFile+"00.skin", m_nIndices, m_ofsIndices); // Useless ?
	m_triangles = new ::Triangles(m_nameFile + "00.skin", m_nTriangles, m_ofsTriangles); // nTriangles is the number of vertices, not of triangles

	m_verticesSize = m_nTriangles * 3 * sizeof(float); 
	m_textureCoordsSize = m_nTriangles * 2 * sizeof(float);
}

void M2Data::CreateOrderedVertices()
{
	m_orderedVertices = new float[m_nTriangles*3]; // nTriangles is the number of vertices, not of triangles
	m_orderedTexturesCoords = new float[m_nTriangles*2];

	for (int i = 0; i < m_nTriangles/3; i++) // For each triangles
	{
		m_orderedVertices[i * 9]     = m_vertices->m_position[m_triangles->m_trianglesList[i].i1 * 3]; // Vertex 1
		m_orderedVertices[i * 9 + 1] = m_vertices->m_position[m_triangles->m_trianglesList[i].i1 * 3 + 1];
		m_orderedVertices[i * 9 + 2] = m_vertices->m_position[m_triangles->m_trianglesList[i].i1 * 3 + 2];

		m_orderedVertices[i * 9 + 3] = m_vertices->m_position[m_triangles->m_trianglesList[i].i2 * 3]; // Vertex 2
		m_orderedVertices[i * 9 + 4] = m_vertices->m_position[m_triangles->m_trianglesList[i].i2 * 3 + 1];
		m_orderedVertices[i * 9 + 5] = m_vertices->m_position[m_triangles->m_trianglesList[i].i2 * 3 + 2];

		m_orderedVertices[i * 9 + 6] = m_vertices->m_position[m_triangles->m_trianglesList[i].i3 * 3]; // Vertex 3
		m_orderedVertices[i * 9 + 7] = m_vertices->m_position[m_triangles->m_trianglesList[i].i3 * 3 + 1];
		m_orderedVertices[i * 9 + 8] = m_vertices->m_position[m_triangles->m_trianglesList[i].i3 * 3 + 2];

		m_orderedTexturesCoords[i * 6]     = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i1 * 2]; // Coord 1
		m_orderedTexturesCoords[i * 6 + 1] = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i1 * 2 + 1];

		m_orderedTexturesCoords[i * 6 + 2] = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i2 * 2]; // Coord 2
		m_orderedTexturesCoords[i * 6 + 3] = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i2 * 2 + 1];

		m_orderedTexturesCoords[i * 6 + 4] = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i3 * 2]; // Coord 3
		m_orderedTexturesCoords[i * 6 + 5] = m_vertices->m_textureCoords[m_triangles->m_trianglesList[i].i3 * 2 + 1];
	}

	/*for (int i = 0; i < m_nTriangles/3; i++)
	{
		cout << "Triangles " << i << endl;
		cout << "i1 : " << m_triangles->m_trianglesList[i].i1 << endl;
		cout << "i2 : " << m_triangles->m_trianglesList[i].i2 << endl;
		cout << "i3 : " << m_triangles->m_trianglesList[i].i3 << endl;
		cout << endl;
	}

	for (int i = 0; i < m_nTriangles/3; i++)
	{
		cout << "Triangles " << i << endl;
		cout << "C1 x : " << m_orderedTexturesCoords[i * 6] << " y :"<< m_orderedTexturesCoords[i * 6 + 1] << endl;
		cout << "C2 x : " << m_orderedTexturesCoords[i * 6 + 2] << " y :" << m_orderedTexturesCoords[i * 6 + 3] << endl;
		cout << "C3 x : " << m_orderedTexturesCoords[i * 6 + 4] << " y :" << m_orderedTexturesCoords[i * 6 + 5] << endl;
		cout << endl;
	}*/
}

string M2Data::GetPathTexture()
{
	return m_textures->m_texturesList[0].filename;
}