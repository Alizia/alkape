#include "adt.h"

ADT::ADT(string nameFile, float scale, string const vertexShader, string const fragmentShader)
{
	m_shader = ShaderTuto(vertexShader, fragmentShader);
	m_shader.charger();

	m_vboID = 0;
	m_vaoID = 0;

	m_nameFile = nameFile;

	m_verticesSize = 256 * 9 * 256 * sizeof(float);
	m_textureCoordsSize = 256 * 6 * 256 * sizeof(float);

	ExtractData();
}

ADT::~ADT()
{
	delete m_mver;
	delete m_mhdr;
	delete m_mcin;
	delete m_mtex;
	delete m_mmdx;
	delete m_mmid;
	delete m_mwmo;
	delete m_mwid;
	delete m_mddf;
	delete m_modf;

	delete[] m_orderedVertices;
	delete[] m_orderedTexturesCoords;

	// VBO Destruction 
	glDeleteBuffers(1, &m_vboID);

	// VAO Destruction
	glDeleteVertexArrays(1, &m_vaoID);
}

ADT::ADT(ADT const& origine) // Copy constructor
{
	m_shader = ShaderTuto(origine.m_shader.m_vertexSource, origine.m_shader.m_fragmentSource);
	m_shader.charger();

	m_nameFile = origine.m_nameFile;
	m_vboID = origine.m_vboID;

	m_mver = new ADT_MVER(*origine.m_mver);
	m_mhdr = new ADT_MHDR(*origine.m_mhdr);
	m_mcin = new ADT_MCIN(*origine.m_mcin);
	m_mtex = new ADT_MTEX(*origine.m_mtex);
	m_mmdx = new ADT_MMDX(*origine.m_mmdx);
	m_mmid = new ADT_MMID(*origine.m_mmid);
	m_mwmo = new ADT_MWMO(*origine.m_mwmo);
	m_mwid = new ADT_MWID(*origine.m_mwid);
	m_mddf = new ADT_MDDF(*origine.m_mddf);
	m_modf = new ADT_MODF(*origine.m_modf);
	m_mcnk = origine.m_mcnk;
	
	m_orderedVertices = new float[256 * 9 * 256]; // 256 triangles par chunk, 256 chunk
	m_orderedTexturesCoords = new float[256 * 6 * 256];

	for (int i = 0; i < (256 * 9 * 256); i++)
	{
		m_orderedVertices[i] = origine.m_orderedVertices[i];
	}

	for (int i = 0; i < (256 * 6 * 256); i++)
	{
		m_orderedTexturesCoords[i] = origine.m_orderedTexturesCoords[i];
	}

	m_verticesSize = origine.m_verticesSize;
	m_textureCoordsSize = origine.m_textureCoordsSize;
}

ADT & ADT::operator=(const ADT & origine) // Operator =
{
	m_shader = ShaderTuto(origine.m_shader.m_vertexSource, origine.m_shader.m_fragmentSource);
	m_shader.charger();
	
	delete m_mver;
	delete m_mhdr;
	delete m_mcin;
	delete m_mtex;
	delete m_mmdx;
	delete m_mmid;
	delete m_mwmo;
	delete m_mwid;
	delete m_mddf;
	delete m_modf;

	delete[] m_orderedVertices;
	delete[] m_orderedTexturesCoords;

	m_nameFile = origine.m_nameFile;
	m_vboID = origine.m_vboID;

	m_mver = new ADT_MVER(*origine.m_mver);
	m_mhdr = new ADT_MHDR(*origine.m_mhdr);
	m_mcin = new ADT_MCIN(*origine.m_mcin);
	m_mtex = new ADT_MTEX(*origine.m_mtex);
	m_mmdx = new ADT_MMDX(*origine.m_mmdx);
	m_mmid = new ADT_MMID(*origine.m_mmid);
	m_mwmo = new ADT_MWMO(*origine.m_mwmo);
	m_mwid = new ADT_MWID(*origine.m_mwid);
	m_mddf = new ADT_MDDF(*origine.m_mddf);
	m_modf = new ADT_MODF(*origine.m_modf);
	m_mcnk = origine.m_mcnk;
	
	m_orderedVertices = new float[256 * 9 * 256]; // 256 triangles par chunk, 256 chunk
	m_orderedTexturesCoords = new float[256 * 6 * 256];

	for (int i = 0; i < (256 * 9 * 256); i++)
	{
		m_orderedVertices[i] = origine.m_orderedVertices[i];
	}

	for (int i = 0; i < (256 * 6 * 256); i++)
	{
		m_orderedTexturesCoords[i] = origine.m_orderedTexturesCoords[i];
	}

	m_verticesSize = origine.m_verticesSize;
	m_textureCoordsSize = origine.m_textureCoordsSize;

	return *this;
}

void ADT::Load()
{
	//cout << "Bite 1" << endl;

	// Destruction of a potential old VBO
	if (glIsBuffer(m_vboID) == GL_TRUE)
		glDeleteBuffers(1, &m_vboID);

	//cout << "Bite 2" << endl;

	// ID generation
	glGenBuffers(1, &m_vboID);

	//cout << "Bite 3" << endl;

	// Lock VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

	//cout << "Bite 4"  << endl;

	// Allocation video memory

	//cout << "m_verticesSize + m_textureCoordsSize : " << m_verticesSize + m_textureCoordsSize << endl;
	//system("PAUSE");

	glBufferData(GL_ARRAY_BUFFER, m_verticesSize + m_textureCoordsSize, 0, GL_STATIC_DRAW);

	//cout << "Bite 5" << endl;

	// Data transfer
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_verticesSize, m_orderedVertices);
	glBufferSubData(GL_ARRAY_BUFFER, m_verticesSize, m_textureCoordsSize, m_orderedTexturesCoords);

	//cout << "Bite 6" << endl;

	// Unlock VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//cout << "Bite 7" << endl;
}

void ADT::Show(mat4 & projection, mat4 & modelview, RessourcesManager & RManager)
{
	//cout << "Bite 0" << endl;
	// Shader activation
	glUseProgram(m_shader.GetProgramID());

	//cout << "Bite 1" << endl;

	// Lock VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

	//cout << "Bite 2" << endl;

	// Access to vertices in video memory
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(0);

	//cout << "Bite 3" << endl;

	// Send textures coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, 0, BUFFER_OFFSET(m_verticesSize));
	glEnableVertexAttribArray(2);

	//cout << "Bite 4" << endl;

	// Unlock VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//cout << "Bite 5" << endl;

	//cout << "ProgramID " << m_shader.GetProgramID() << endl;

	// Send matrices

	glUniformMatrix4fv(glGetUniformLocation(m_shader.GetProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.GetProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

	//cout << "Bite 6" << endl;

	// Lock texture (the first one)
	Texture::bind(&RManager.GetTexture("TILESET/EmeraldDreamCanyon/DreamCGrass02.png")); // Replace the OpenGL's glBindTexture function

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // To draw a surface with outlined polygons 

	//cout << "Bite 7" << endl;

	// Render
	glDrawArrays(GL_TRIANGLES, 0, 256 * 3 * 256);

	// Unlock texture
	Texture::bind(0); // Replace the OpenGL's glBindTexture function

	//cout << "Bite 8" << endl;

					  // Array deactivation
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(0);


	//cout << "Bite 9" << endl;

	// Shader deactivation
	glUseProgram(0);
}

void ADT::ExtractData()
{
	string identifier;
	identifier.resize(4);

	ifstream adtFile(m_nameFile, ios::in | ios::binary);
	if (!adtFile.is_open())
	{
		cout << "Impossible d'ouvrir le fichier : " << m_nameFile << endl;
		system("PAUSE");
		exit(1);
	}

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MVER
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MVER
	if (identifier != "REVM")
	{
		cout << "Error : Chunk identifier not found MVER : " << identifier << endl;
		system("pause");
	}
	m_mver = new ADT_MVER(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MHDR
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MHDR
	if (identifier != "RDHM")
	{
		cout << "Error : Chunk identifier not found MHDR : " << identifier << endl;
		system("pause");
	}
	m_mhdr = new ADT_MHDR(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MCIN
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MCIN
	if (identifier != "NICM")
	{
		cout << "Error : Chunk identifier not found MCIN : " << identifier << endl;
		system("pause");
	}
	m_mcin = new ADT_MCIN(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MTEX
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MTEX
	if (identifier != "XETM")
	{
		cout << "Error : Chunk identifier not found MTEX : " << identifier << endl;
		system("pause");
	}
	m_mtex = new ADT_MTEX(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MMDX
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MMDX
	if (identifier != "XDMM")
	{
		cout << "Error : Chunk identifier not found MMDX : " << identifier << endl;
		system("pause");
	}
	m_mmdx = new ADT_MMDX(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MMID
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MMID
	if (identifier != "DIMM")
	{
		cout << "Error : Chunk identifier not found MMID : " << identifier << endl;
		system("pause");
	}
	m_mmid = new ADT_MMID(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MWMO
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MWMO
	if (identifier != "OMWM")
	{
		cout << "Error : Chunk identifier not found MWMO : " << identifier << endl;
		system("pause");
	}
	m_mwmo = new ADT_MWMO(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MWID
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MWID
	if (identifier != "DIWM")
	{
		cout << "Error : Chunk identifier not found MWID : " << identifier << endl;
		system("pause");
	}
	m_mwid = new ADT_MWID(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MDDF
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MDDF
	if (identifier != "FDDM")
	{
		cout << "Error : Chunk identifier not found MDDF : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
		system("pause");
	}
	m_mddf = new ADT_MDDF(adtFile);

	//adtFile.seekg((::uint32_t)adtFile.tellg() + sizeof(::uint32_t)); // MODF
	adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MODF
	if (identifier != "FDOM")
	{
		cout << "Error : Chunk identifier not found MODF : " << identifier << endl;
		system("pause");
	}
	m_modf = new ADT_MODF(adtFile);

	//cout << "Bite 1 tellg : " << adtFile.tellg() << endl;

	::uint32_t nextMcnkOfs;
	for (int i = 0; i < 256; i++)
	{
		//cout << "MCNK_" << dec << i << endl;
		//cout << " tellg : " << hex << adtFile.tellg() << endl;
		adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MCNK
		if (identifier != "KNCM")
		{
			if (identifier == "O2HM")
			{
				adtFile.seekg(m_mcin->GetFirstOfsMCNK()); // go to the first MCNK, ignore MH20
				adtFile.read((char*)identifier.data(), sizeof(::uint32_t)); // MCNK
			}

			if (identifier != "KNCM")
			{
				cout << "Error : Chunk identifier not found MCNK : " << identifier << " tellg : " << hex << adtFile.tellg() << endl;
				system("pause");
			}
		}

		::uint32_t size;
		adtFile.read((char*)&size, sizeof(size));
		//cout << "tellg : " << hex << adtFile.tellg() << " + size : " << hex << size << endl;
		nextMcnkOfs = (::uint32_t)adtFile.tellg() + size;

		ADT_MCNK mcnk(adtFile);
		m_mcnk.push_back(mcnk);

		adtFile.seekg(nextMcnkOfs);
	}

	//system("PAUSE");

	adtFile.close();

	CreateOrderedVertices();
}

void ADT::AddVertices(float* orderedVertices, ::uint32_t &index, float x, float y, float z)
{
	orderedVertices[index] = x;
	orderedVertices[index + 1] = y;
	orderedVertices[index + 2] = z;
	index += 3;
}

void ADT::AddTextureCoords(float * orderedTextureCoords, ::uint32_t &index, float x, float y)
{
	orderedTextureCoords[index] = x;
	orderedTextureCoords[index + 1] = y;
	index += 2;
}

void ADT::CreateOrderedVertices()
{
	m_orderedVertices = new float[256 * 9 * 256]; // 256 triangles par chunk, 256 chunk
	m_orderedTexturesCoords = new float[256 * 6 * 256];

	//Vector3f pos(-3200.00f, 3733.33f, 113.275f);

	::uint32_t iV = 0;
	::uint32_t iT = 0;
	float scale = 4.1666625f;

	for (int i = 0; i < 256; i++) // For every mcnk chunk
	{
		Vector3f pos = m_mcnk[i].GetPosition();

		if (i == 0)
		{
			//cout << "Bite GetPosition() pos : " << to_string(pos.x) << " " << to_string(pos.y) << " " << to_string(pos.z) << " " << endl; 
			//cout << "Bite GetPosition() pos : " << pos.x << " " << pos.y << " " << pos.z << " " << endl;
		}

		for (int x = 0; x < 8; x++)
		{
			for (int y = 0; y < 8; y++)
			{
				float nl1 = m_mcnk[i].GetValNoLOD(x, y);
				float nl2 = m_mcnk[i].GetValNoLOD(x+1, y);
				float nl3 = m_mcnk[i].GetValNoLOD(x+1, y+1);
				float nl4 = m_mcnk[i].GetValNoLOD(x, y+1);
				float l = m_mcnk[i].GetValLOD(x, y);


				if (i == 0 && x == 0 && y == 0)
				{
					//cout << "Bite 2 pos : " << to_string(pos.x) << " " << to_string(pos.y) << " " << to_string(pos.z + nl1) << " " << endl; // For manual camera position
				}

				// Tri A
				AddVertices(m_orderedVertices, iV, pos.x + (x)*scale, pos.y - (y)*scale, pos.z + nl1); // H1
				AddVertices(m_orderedVertices, iV, pos.x + (x+1)*scale, pos.y - (y)*scale, pos.z + nl2); // H2
				AddVertices(m_orderedVertices, iV, pos.x + (x + 0.5f)*scale, pos.y - (y + 0.5f)*scale, pos.z + l); //H5

				AddTextureCoords(m_orderedTexturesCoords, iT, 0, 1); // Tex Tri 1
				AddTextureCoords(m_orderedTexturesCoords, iT, 1, 1);
				AddTextureCoords(m_orderedTexturesCoords, iT, 0.5f, 0.5f);

				// Tri B
				AddVertices(m_orderedVertices, iV, pos.x + (x+1)*scale, pos.y - (y)*scale, pos.z + nl2); // H2
				AddVertices(m_orderedVertices, iV, pos.x + (x+1)*scale, pos.y - (y + 1)*scale, pos.z + nl3); // H3
				AddVertices(m_orderedVertices, iV, pos.x + (x + 0.5f)*scale, pos.y - (y + 0.5f)*scale, pos.z + l); //H5

				AddTextureCoords(m_orderedTexturesCoords, iT, 1, 1); // Tex Tri 2
				AddTextureCoords(m_orderedTexturesCoords, iT, 1, 0);
				AddTextureCoords(m_orderedTexturesCoords, iT, 0.5f, 0.5f);

				// Tri C
				AddVertices(m_orderedVertices, iV, pos.x + (x+1)*scale, pos.y - (y + 1)*scale, pos.z + nl3); // H3
				AddVertices(m_orderedVertices, iV, pos.x + (x)*scale, pos.y - (y + 1)*scale, pos.z + nl4); // H4
				AddVertices(m_orderedVertices, iV, pos.x + (x + 0.5f)*scale, pos.y - (y + 0.5f)*scale, pos.z + l); //H5

				AddTextureCoords(m_orderedTexturesCoords, iT, 0, 0); // Tex Tri 3
				AddTextureCoords(m_orderedTexturesCoords, iT, 1, 0);
				AddTextureCoords(m_orderedTexturesCoords, iT, 0.5f, 0.5f);

				// Tri D
				AddVertices(m_orderedVertices, iV, pos.x + (x)*scale, pos.y - (y + 1)*scale, pos.z + nl4); // H4
				AddVertices(m_orderedVertices, iV, pos.x + (x)*scale, pos.y - (y)*scale, pos.z + nl1); // H1
				AddVertices(m_orderedVertices, iV, pos.x + (x + 0.5f)*scale, pos.y - (y + 0.5f)*scale, pos.z + l); //H5

				AddTextureCoords(m_orderedTexturesCoords, iT, 0, 0); // Tex Tri 4
				AddTextureCoords(m_orderedTexturesCoords, iT, 0, 1);
				AddTextureCoords(m_orderedTexturesCoords, iT, 0.5f, 0.5f);
			}
		}
	}
}

Vector3f ADT::GetMcnkPos(int mcnkId)
{
	//cout << "m_mcnk size : " << m_mcnk.size() << endl; 
	return m_mcnk[mcnkId].GetPosition();
}

void ADT::ShowAllInfos()
{
	cout << endl;
	cout << "Show All Infos : " << endl;
	cout << "m_nameFile : " << m_nameFile << endl;
	//cout << "m_shader.m_vertexSource : " << m_shader.m_vertexSource << endl;
	//cout << "m_shader.m_fragmentSource : " << m_shader.m_fragmentSource << endl;
	cout << "m_shader.m_vertexID : " << m_shader.GetVertexID() << endl;
	cout << "m_shader.m_fragmentID : " << m_shader.GetFragmentID() << endl;
	cout << "m_shader.m_programID : " << m_shader.GetProgramID() << endl;
	cout << "m_vboID : " << m_vboID << endl;
	/*cout << "m_orderedVertices : " << endl;
	for (int i = 0; i < 9; i++)
	{
		cout << m_orderedVertices[i] << endl;
	}
	for (int i = 0; i < 9; i++)
	{
		cout << m_orderedTexturesCoords[i] << endl;
	}
	cout << "m_verticesSize : " << m_verticesSize << endl;
	cout << "m_textureCoordsSize : " << m_textureCoordsSize << endl;*/
}
