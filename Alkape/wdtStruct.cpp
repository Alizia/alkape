#include "wdtStruct.h"

WDT_MVER::WDT_MVER(ifstream &wdtFile)
{
	wdtFile.read((char*)&m_size, sizeof(m_size));
	wdtFile.read((char*)&m_version, sizeof(m_version));
}

WDT_MVER::~WDT_MVER()
{
}

WDT_MPHD::WDT_MPHD(ifstream &wdtFile)
{
	wdtFile.read((char*)&m_size, sizeof(m_size));
	wdtFile.read((char*)&m_flags, sizeof(m_flags));
	wdtFile.read((char*)&m_something, sizeof(m_something));
	wdtFile.read((char*)&m_unused, sizeof(m_unused));
}

WDT_MPHD::~WDT_MPHD()
{
}

WDT_MAIN::WDT_MAIN(ifstream &wdtFile)
{
	wdtFile.read((char*)&m_size, sizeof(m_size));
	
	::uint32_t width = GetWidthMap(); // width = height, only square map

	for (int i = 0; i < width; i++) // x and y coords reversed in the file... Fuck you Blizzard.
	{
		vector<::uint32_t> row;
		m_mapAreaInfos.push_back(row);
	}
	
	for (int j = 0; j < width; j++)
	{
		for (int i = 0; i < width; i++)
		{
			::uint32_t mapAreaInfo;
			wdtFile.read((char*)&mapAreaInfo, sizeof(mapAreaInfo));
			m_mapAreaInfos[i].push_back(mapAreaInfo);

			wdtFile.seekg((::uint32_t)wdtFile.tellg() + sizeof(::uint32_t)); // m_area unused
		}
	}
}

WDT_MAIN::~WDT_MAIN()
{
}

::uint32_t WDT_MAIN::GetWidthMap()
{
	return lround(sqrt(m_size / 8)); // Round... Just in case. Murphy stuff.
}

::uint32_t WDT_MAIN::GetFlags(int x, int y)
{
	return m_mapAreaInfos[x][y];
}

WDT_MWMO::WDT_MWMO(ifstream &wdtFile)
{
	::uint32_t size;
	wdtFile.read((char*)&size, sizeof(size));

	if (size > 0)
	{
		m_wmoFileName.resize(size);
		wdtFile.read(&m_wmoFileName[0], size);
	}
}

WDT_MWMO::~WDT_MWMO()
{
}

::uint32_t WDT_MWMO::GetSize()
{
	return m_size;
}

string WDT_MWMO::GetWmoFileName()
{
	return m_wmoFileName;
}

WDT_MODF::WDT_MODF(ifstream &wdtFile)
{
	::uint32_t size; // unused, always 64 ?
	wdtFile.read((char*)&size, sizeof(size));

	wdtFile.read((char*)&m_nameId, sizeof(m_nameId));
	wdtFile.read((char*)&m_uniqueId, sizeof(m_uniqueId));
	wdtFile.read((char*)&m_position, sizeof(m_position));
	wdtFile.read((char*)&m_orientation, sizeof(m_orientation));
	wdtFile.read((char*)&m_upperExtents, sizeof(m_upperExtents));
	wdtFile.read((char*)&m_lowerExtents, sizeof(m_lowerExtents));
	wdtFile.read((char*)&m_flags, sizeof(m_flags));
	wdtFile.read((char*)&m_doodadSetIndex, sizeof(m_doodadSetIndex));
	wdtFile.read((char*)&m_padding, sizeof(m_padding));
}

WDT_MODF::~WDT_MODF()
{
}