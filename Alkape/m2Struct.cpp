#include "m2Struct.h"

// DATA M2

Vertices::Vertices(string nameFile, ::uint32_t nVertices, ::uint32_t ofsVertices)
{
	ifstream m2File(nameFile, ios::in | ios::binary);
	m2File.seekg(ofsVertices);

	m_position	    = new float[nVertices * 3];
	m_boneWeight	= new uint8_t[nVertices * 4];
	m_boneIndices	= new uint8_t[nVertices * 4];
	m_normal		= new float[nVertices * 3];
	m_textureCoords = new float[nVertices * 2];
	m_unknown		= new float[nVertices * 2];

	for (int i = 0; i < nVertices; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			m2File.read((char*)&m_position[i*3+j], sizeof(float));
		}
		for (int j = 0; j < 4; j++)
		{
			m2File.read((char*)&m_boneWeight[i * 4 + j], sizeof(uint8_t));
		}
		for (int j = 0; j < 4; j++)
		{
			m2File.read((char*)&m_boneIndices[i *4 + j], sizeof(uint8_t));
		}
		for (int j = 0; j < 3; j++)
		{
			m2File.read((char*)&m_normal[i * 3 + j], sizeof(float));
		}
		for (int j = 0; j < 2; j++)
		{
			m2File.read((char*)&m_textureCoords[i * 2 + j], sizeof(float));
		}
		for (int j = 0; j < 2; j++)
		{
			m2File.read((char*)&m_unknown[i * 2 + j], sizeof(float));
		}
	}

	m2File.close();
}

Vertices::~Vertices()
{
	delete[] m_position;
	delete[] m_boneWeight;
	delete[] m_boneIndices;
	delete[] m_normal;
	delete[] m_textureCoords;
	delete[] m_unknown;
}

Textures::Textures(string nameFile, ::uint32_t nTextures, ::uint32_t ofsTextures)
{
	ifstream m2File(nameFile, ios::in | ios::binary);
	m2File.seekg(ofsTextures);

	for (int i = 0; i < nTextures; i++)
	{
		m2texture m2tex;
		texture tex;

		m2File.read((char*)&m2tex, sizeof(m2tex));

		tex.flags = m2tex.flags;
		tex.type = m2tex.type;
		ReadString(tex.filename, m2File, m2tex.ofsFilename, m2tex.lenFilename);

		StrReplace(tex.filename, ".BLP", ".png"); // Manual convertion with BLP2PNG, to do : automatic convertion

		m_texturesList.push_back(tex);
	}

	m2File.close();
}

void Textures::ReadString(string &string, ifstream &stream, ::uint32_t offset, ::uint32_t length)
{
	::uint32_t jumpBack = stream.tellg();
	stream.seekg(offset); // Jump to where the texture is

	string.resize(length);
	stream.read(&string[0], length);

	stream.seekg(jumpBack); // jump back where we were;
}

Textures::~Textures()
{

}


// DATA SKIN

Indices::Indices(string nameFile, ::uint32_t nIndices, ::uint32_t ofsIndices)
{
	ifstream skinFile(nameFile, ios::in | ios::binary);
	skinFile.seekg(ofsIndices);

	for (int i = 0; i < nIndices; i++)
	{
		::int16_t indice;
		skinFile.read((char*)&indice, sizeof(::int16_t));
		m_indexVertices.push_back(indice);
	}

	skinFile.close();
}

Indices::~Indices()
{

}


Triangles::Triangles(string nameFile, ::uint32_t nTriangles, ::uint32_t ofsTriangles)
{
	ifstream skinFile(nameFile, ios::in | ios::binary);
	skinFile.seekg(ofsTriangles);

	for (int i = 0; i < nTriangles/3; i++) // nTriangles is the number of vertices, not of triangles
	{
		Triangle tri;
		skinFile.read((char*)&tri, sizeof(Triangle));
		m_trianglesList.push_back(tri);
	}

	skinFile.close();
}

Triangles::~Triangles()
{

}