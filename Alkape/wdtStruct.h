#pragma once

#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <cmath>

#include <SFML\System.hpp>

using namespace std;
using namespace sf;

class WDT_MVER
{
	public:
		WDT_MVER(ifstream &wdtFile);
		~WDT_MVER();

	private:
		::uint32_t m_size; // useless, always 0x04 ?
		::uint32_t m_version;
};

class WDT_MPHD
{
	public:
		WDT_MPHD(ifstream &wdtFile);
		~WDT_MPHD();

	private:
		::uint32_t m_size; // useless, always 0x20 ?
		::uint32_t m_something;
		::uint32_t m_flags;
		::uint32_t m_unused[6];
};

class WDT_MAIN
{
	public:
		WDT_MAIN(ifstream &wdtFile);
		~WDT_MAIN();

		::uint32_t GetWidthMap();
		::uint32_t GetFlags(int x, int y);

	private:
		::uint32_t m_size;
		vector<vector<::uint32_t>> m_mapAreaInfos;
};

class WDT_MWMO
{
	public:
		WDT_MWMO(ifstream &wdtFile);
		~WDT_MWMO();

		::uint32_t GetSize(); // Only if no adt on the map
		string GetWmoFileName();

	private:
		::uint32_t m_size;
		string m_wmoFileName;
};

class WDT_MODF // Only if no adt on the map
{
	public:
		WDT_MODF(ifstream &wdtFile);
		~WDT_MODF();

	private:
		::uint32_t m_nameId;
		::uint32_t m_uniqueId;
		Vector3f m_position;
		Vector3f m_orientation;
		float m_upperExtents[3];
		float m_lowerExtents[3];
		::uint16_t m_flags;
		::uint16_t m_doodadSetIndex;
		::uint16_t m_nameSet;
		::uint16_t m_padding;
};