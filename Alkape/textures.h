#pragma once

#include "ressourcesManager.h"
#include <string>
#include <iostream>

using namespace std;
using namespace sf;

void addTexture(string strTexture, RessourcesManager &RManager, bool repeated = false);