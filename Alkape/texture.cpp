#include "textures.h"

void addTexture(string strTexture, RessourcesManager &RManager, bool repeated /*= false*/)
{
	Image im;
	Texture texture;

	if (texture.create(64, 64))
	{
		if (!im.loadFromFile("mpq/" + strTexture)) // if image don't exist
		{
			cout << "Err : Texture " << strTexture << " missing" << endl;
			system("PAUSE");
			return;
		}
		im.flipHorizontally();
		//im.flipVertically();

		texture.loadFromImage(im);
		texture.setRepeated(repeated);

		RManager.LoadTexture(strTexture, texture);
	}
	else
	{
		cout << "Error : Texture creation failed !" << endl;
	}
}