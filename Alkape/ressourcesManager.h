#pragma once

#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class RessourcesManager
{
	public:
		RessourcesManager(){}
		~RessourcesManager() {}

		void LoadTexture(string name, Texture tex);
		Texture &GetTexture(string name);

	private:
		map<string, Texture> _textures;
};