#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#include <stdint.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>

#include "adtStruct.h"
#include "shader.h"
#include "ressourcesManager.h"

using namespace std;
using namespace glm;

// Macro utile au VBO
#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif

class ADT
{
	public:
		ADT(string nameFile, float scale, string const vertexShader, string const fragmentShader);
		~ADT();

		ADT(const ADT & origine);
		ADT & ADT::operator=(const ADT & origine);

		void Load();
		void Show(mat4 &projection, mat4 &modelview, RessourcesManager &RManager);

		void ExtractData();
		void AddVertices(float* orderedVertices, ::uint32_t &index, float x, float y, float z);
		void AddTextureCoords(float* orderedTextureCoords, ::uint32_t &index, float x, float y);
		void CreateOrderedVertices();

		Vector3f GetMcnkPos(int mcnkId); // 119 for middle of the adt

		//debug
		void ShowAllInfos();
		string m_nameFile;

	private:
		
		ShaderTuto m_shader;
		GLuint m_vboID;
		GLuint m_vaoID;

		float* m_orderedVertices;
		float* m_orderedTexturesCoords;
		::int32_t m_verticesSize;
		::int32_t m_textureCoordsSize;

		ADT_MVER* m_mver;
		ADT_MHDR* m_mhdr;
		ADT_MCIN* m_mcin;
		ADT_MTEX* m_mtex;
		ADT_MMDX* m_mmdx;
		ADT_MMID* m_mmid;
		ADT_MWMO* m_mwmo;
		ADT_MWID* m_mwid;
		ADT_MDDF* m_mddf;
		ADT_MODF* m_modf;
		vector<ADT_MCNK> m_mcnk;
};
